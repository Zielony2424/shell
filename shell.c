#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "logger.h"


typedef struct{
	const char* name;
	int (*function)(char** args);
} funStruct;

int shell_builtin_cd(char** args){
	if(args[1] == NULL){
		fprintf(stderr, "Expected argument to cd\n");
		return 1;
	}
	if(chdir(args[1])){
		perror("cd");
		return 2;
	}
	
}

char* shell_read_line(){
	char* line = NULL;
	char pathname[PATH_MAX];
	size_t buffer_size_returned = 0;
	logger(DEBUG, "Getting working path");
	if(getcwd(pathname, sizeof(pathname)) != NULL){
		printf("%s", pathname);
//		getline(&line, &buffer_size_returned, stdin);
		line = readline(" > ");
		add_history(line);
	}
	else{
		perror("shell");
		exit(EXIT_FAILURE);
	}
	return line;
}

char** shell_split_line(char* line){
	#define TOKENS_BUFFER 64
	#define DELIMS " \t\r\n\a"
	int buffer_size = TOKENS_BUFFER, position = 0;
	char ** tokens = malloc(sizeof(char*)*TOKENS_BUFFER);	
	char* token;
	//TODO Check malloc;
	token = strtok(line, DELIMS);
	while(token != NULL){
		tokens[position] = token;
		position++;
		if(position >= buffer_size){
			tokens = realloc(tokens, buffer_size * sizeof(char*));
			//TODO handle errors;
		}
		token = strtok(NULL, DELIMS);
	}
	tokens[position] = NULL;
	return tokens;	
}

int shell_execute(char** args){
	pid_t pid;
	int status;
	pid = fork();
	if(pid < 0){
		perror("shell:");
		exit(EXIT_FAILURE);
	}
	if(pid == 0){ //child
		if(execvp(args[0], args)){
			perror("shell:");
		}
	}
	else{ //parent
		do{
			waitpid(pid, &status, WUNTRACED); 
		}while(!WIFEXITED(status) && !WIFSIGNALED(status));
	}
	return 1;

}

int shell_launch(char** args){
	if(args[0] == NULL){
		return 1;
	}
	funStruct shell_builtin_functions[] = {
		{"cd", &shell_builtin_cd}
	};
	size_t number_of_builtins = sizeof(shell_builtin_functions)/sizeof(funStruct);
	for(int i = 0; i <number_of_builtins; ++i){
		if(strcmp(shell_builtin_functions[i].name, args[0]) == 0){
			shell_builtin_functions[i].function(args);
			return 0;
		}
	}
	shell_execute(args);
	return 0;
	
}

int shell_loop(){
	char* line;
	char** args;
	while(1){
		line = shell_read_line();
		args = shell_split_line(line);
		shell_launch(args);
		free(args);
		free(line);
	}
}

int main(){
	logger_set_threshold(ERROR);
	shell_loop();
	return EXIT_SUCCESS;
}
